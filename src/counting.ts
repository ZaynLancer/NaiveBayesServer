import { MongoClient, Db, ObjectId } from "mongodb";
import { BayesClassifier } from "./lib/bayes-classifier";
import { loging } from "./lib/loging";
import { Stemmer } from "./lib/stemmer";


// URI String
let uri: string = 'mongodb://127.0.0.1:8800'
let timer: number = 0


// Informations Variables
loging('Counting is started')

let informations: Informations = {
    ganjar: {},
    sudirman: {}
}

let t = setInterval(() => {
    timer = timer + 0.1
}, 100);

let classifier = new BayesClassifier(
    new Stemmer('dictionary.json'), 'naive-document.json'
)

classifier.train()

{

    /**
     * Connection
     */
    MongoClient.connect(uri, {
        native_parser: true
    },(err, client) => {

        if(err) throw(err);
        
        let db: Db = client.db('naive')

        loging('Fetch data from collection replies_ganjar')

        /**
         * Operations Database for counting
         */
        db.collection('replies_ganjar')
        .find()
        .project({
            id: 1,
            id_str: 1,
            in_reply_to_status_id: 1,
            in_reply_to_status_id_str: 1,
            in_reply_to_user_id: 1,
            in_reply_to_user_id_str: 1,
            created_at : 1,
            text: 1
        })
        .sort({
            'id': 1
        })
        .toArray()
        .then(results => {

            loging('Fetched')

            /**
             * Set Data Informations
             */
            let ganjarToUser: Info = new CInfo()
            
            let ganjarToStatus: Info = new CInfo()
            
            let ganjarToAt: Info = new CInfo()

            
            // Save this counting in collection information
            // Statuses Counting
            /**
             * Replies Ganjar Pranowo
             */

            let count = results.length

            loging('Counting Replies')

            for (let i = 0; i < count; i++) {

                let item = results[i]

                /**
                 * Status Reply should be in first places
                 */
                if(item.in_reply_to_status_id && item.in_reply_to_status_id_str){
                    
                    setInfo(item, ganjarToStatus)

                }else if(item.in_reply_to_user_id && item.in_reply_to_user_id_str){

                    setInfo(item, ganjarToUser)

                }else{

                    setInfo(item, ganjarToAt)

                }

            }

            loging('Finish Count Replies ganjar')

            let replies: Replies = {
                to_at: ganjarToAt,
                to_status: ganjarToStatus,
                to_user: ganjarToUser,
                count: count
            }

            informations.ganjar.replies = replies

            loging('Fetch data from collection replies_sudirman')

            return db.collection('replies_sudirman')
            .find()
            .project({
                id: 1,
                id_str: 1,
                in_reply_to_status_id: 1,
                in_reply_to_status_id_str: 1,
                in_reply_to_user_id: 1,
                in_reply_to_user_id_str: 1,
                created_at : 1,
                text: 1
            })
            .sort({
                'id': 1
            })
            .toArray()
        })
        .then((results) => {

            loging('Fetched replies_sudirman')

            let sudirmanToStatus: Info = new CInfo()

            let sudirmanToUser: Info = new CInfo()

            let sudirmanToAt: Info = new CInfo()


            // Statuses Counting
            /**
             * Replies Sudirman Said
             */
            
            loging('Counting Replies')

            let count = results.length

            for (let i = 0; i < count; i++) {
                
                let item = results[i]

                /**
                 * Status Reply should be in first places
                 */
                if(item.in_reply_to_status_id && item.in_reply_to_status_id_str){
                    
                    setInfo(item, sudirmanToStatus)

                }else if(item.in_reply_to_user_id && item.in_reply_to_user_id_str){

                    setInfo(item, sudirmanToUser)

                }else{

                    setInfo(item, sudirmanToAt)

                }

            }

            loging('Finish Count replies_sudirman')

            let replise: Replies = {
                to_at: sudirmanToAt,
                to_status: sudirmanToStatus,
                to_user: sudirmanToUser,
                count: count
            }

            informations.sudirman.replies = replise

            loging('Fetch data from collection tweeets_ganjar')
            
            return db.collection('tweets_ganjar')
            .find()
            .project({
                id: 1,
                id_str: 1,
                created_at : 1,
                text: 1
            })
            .sort({
                'id': 1
            })
            .toArray()
        })
        .then(results => {

            loging('Fetched tweets_ganjar')

            // Save this counting in collection information
            // Tweets Counting
            let ganjarTweets: Info = new CInfo()

            let count = results.length

            loging('Counting tweets')

            for (let i = 0; i < count; i++) {
                
                setInfo(results[i], ganjarTweets)

            }

            loging('Finish count tweets ganjar')

            informations.ganjar.tweets = ganjarTweets

            loging('Fetch data from collection tweets_sudirman')

            return db.collection('tweets_sudirman')
            .find()
            .project({
                id: 1,
                id_str: 1,
                created_at : 1,
                text: 1
            })
            .sort({
                'id': 1
            })
            .toArray()
        })
        .then(results => {

            loging('Fetched tweets_sudirman')

            // Save this counting in collection information
            // Tweets Counting
            let sudirmanTweets: Info = new CInfo()

            let count = results.length

            loging('Counting tweets sudirman')

            for (let i = 0; i < count; i++) {

                setInfo(results[i], sudirmanTweets)

            }

            loging('Finish Count tweets sudirman')

            loging('Collecting data for informations')

            informations.sudirman.tweets = sudirmanTweets
            informations.counted_at = new Date().toLocaleString()
            clearTimeout(t)
            informations.time_to_finish = timer

            loging('Insert informations data')

            // Save information to collection information
            // Save Statuses and Tweets counting
            return db.collection('informations').insertOne(informations)
        })
        .then(result => {

            loging('Informations inserted')

            // Operation is DONE
            client.close()
            loging('Operation COMPLETE')
            console.log(timer)
        })
        .catch(err => {

            // Operations error occured
            clearTimeout(t)
            client.close()
            loging(err)
            loging('Operation Error')

        })
    })
}


// Variables Funstions
let setInfo = (item: DbInfo, target: Info) => {

    let isPositive = classifier.classify(item.text) == 'positive'
    let newDate = new Date(item.created_at).toLocaleString()

    target.count++

    target.last_tweet_date = target.last_tweet_date < newDate ?
        newDate : target.last_tweet_date

    if (target.max_id < item.id) {
        target.max_id = item.id
        target.max_id_str = item.id_str
    }
    
    if(isPositive){
        target.positive++
    }else{
        target.negative++
    }

}

// Interfaces

interface DbInfo{
    
    _id: ObjectId
    id: number
    id_str: string
    in_reply_to_status_id: number | null
    in_reply_to_status_id_str: string | null
    in_reply_to_user_id: number | null
    in_reply_to_user_id_str: string | null
    created_at : string
    text: string

}

class CInfo{
    count = 0
    last_tweet_date = ''
    max_id = 0
    max_id_str = ''
    negative = 0
    positive = 0
}

interface Info{

    count: number
    max_id: number
    max_id_str: string
    positive: number
    negative: number
    last_tweet_date: string

}

interface Replies{
    
    to_user: Info
    to_status: Info
    to_at: Info
    count: number

}

interface Information{

    tweets?: Info
    replies?: Replies

}

interface Informations{

    ganjar: Information
    sudirman: Information
    counted_at?: string
    time_to_finish?: number

}