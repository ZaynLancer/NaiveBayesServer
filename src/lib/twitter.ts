import Twit from "twit";
import { readFileSync } from "fs";

let sec: any = JSON.parse(readFileSync('./secret.json', {encoding: "UTF-8"}))

let apiKey = sec.apiKey
let apiSecret = sec.apiSecret
let tokenKey = sec.tokenKey
let tokenSecret = sec.tokenSecret

let options: Twit.Options = {
    access_token: tokenKey,
    access_token_secret: tokenSecret,
    consumer_key: apiKey,
    consumer_secret: apiSecret,
    timeout_ms: 60 * 1000
}

export let Twitter = new Twit(options)
