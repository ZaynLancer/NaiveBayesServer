import { MongoClient } from "mongodb";
import { Twitter } from "./twitter";
import { loging } from "./loging";


// Uri MongoDb
let uri: string = 'mongodb://127.0.0.1:8800'
let max_id = 999629834543235100
let max_id_str: string = '999629834543235073'


// Cloning Funtions
let cloning = (option: {
    dbName: string,
    collection: string,
    collection_info: string,
    screen_name: string,
    count: number
}) => {

    loging('Operation started')
    
    MongoClient.connect(uri, {
        native_parser: true
    })
    .then(client => {
        
        loging('Connected')

        let db = client.db(option.dbName)
        let colInfo = db.collection(option.collection_info)
        let info: any = {}

        colInfo.find().sort({'fetchAt': -1}).limit(1).toArray()
        .then((result: any) => {

            loging('Fetched collection from ' + option.collection_info)

            return Twitter.get('search/tweets', {
                q: option.screen_name,
                result_type: "recent",
                lang: "in",
                since_id: result.length > 0 ? result[0].max_id_str : max_id_str,
                count: option.count
            })

        })
        .then((result: any) => {
            
            loging('Fetch collection from ' + option.collection)

            let col = db.collection(option.collection)

            info.max_id = result.data.search_metadata.max_id
            info.max_id_str = result.data.search_metadata.max_id_str
            info.search_count = result.data.search_metadata.count
            info.count = result.data.statuses.length
            info.since_id = result.data.search_metadata.since_id
            info.since_id_str = result.data.search_metadata.since_id_str
            info.fetchAt = new Date().toLocaleString()
            info.is_over_count = info.count == 100 ? true : false

            if(info.count > 0){
                return col.insertMany(result.data.statuses)
            }else{
                return Promise.resolve({})
            }

        })
        .then(result => {

            loging('Insered Statueses')

            return colInfo.insertOne(info)
        })
        .then(result => {
            
            loging("All Operation is done")

            client.close()
            
        })
        .catch(err => {
            
            loging('Operation error')
            loging(err)

            client.close()
        })

    })
    .catch(err => {
        
        loging('Connection error')

        loging(err)
    })
}


// Replies
export let cloningReplies = (
    option: {
        dbName: string,
        collection: string,
        collection_info: string,
        screen_name: string,
        count: number
    }
) => {

    cloning({
        dbName: option.dbName,
        collection: option.collection,
        collection_info: option.collection_info,
        count: option.count,
        screen_name: "to:" +  option.screen_name
    })

}


// Tweets
export let cloningTweets = (
    option: {
        dbName: string,
        collection: string,
        collection_info: string,
        screen_name: string,
        count: number
    }
) => {

    cloning({
        collection: option.collection,
        collection_info: option.collection_info,
        count: option.count,
        dbName: option.dbName,
        screen_name: 'from:' + option.screen_name
    })

}
